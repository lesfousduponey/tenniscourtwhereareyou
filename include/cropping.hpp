// Copyright 2014 TCWAY
// This file is part of TCWAY (Tennis Court Where Are You).
//
// TCWAY is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TCWAY is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with TCWAY.  If not, see <http://www.gnu.org/licenses/>.

// Author: Christophe S. / Mikael T.


using namespace std;

class cropImage {
public:
  // Constructor
  cropImage(string filename, string postfix, int pixel_to_cut);

  // overload << operator for printing purpose
  friend ostream&
  operator<<(ostream& os, const cropImage& dt);

  // crop the image: it reads, crops and writes.
  int
  cropit();

  // Accessors
  string
  get_filename() const;

  string
  get_postfix() const;

  string
  get_crop_filename() const;

  int
  get_pixcut() const;

  // Setter
  void
  set_crop_filename(string name);

private:
  int pixcut; // Number of pixel to cut at the bottom
  string filename; // Filename of the picture that will be open
  string postfix; // A postfix to add to the filename that will be written
  string crop_filename; // The filename that will be written.
};
