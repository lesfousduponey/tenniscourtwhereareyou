# Copyright 2014 TCWAY
# This file is part of TCWAY (Tennis Court Where Are You).
#
# TCWAY is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TCWAY is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TCWAY.  If not, see <http://www.gnu.org/licenses/>.

# Author: Mikael T., Christophe S.

import math
import os
from GpsCoordinate import *
import sys

import argparse

#### PARSING ####
parser = argparse.ArgumentParser(description='Download Google images of a large area given specific\
	starting coordinates and zoom level.')
parser.add_argument('lat', metavar='latitude',type=float, help='Latitude of starting point')
parser.add_argument('long', metavar='longitude',type=float, help='Longitude of starting point')
parser.add_argument('-z','--zoom', metavar='zoom', type=int, default=19, choices=range(0,21),
                   help='Zoom level for the images (default=19)')
parser.add_argument('-s','--size', metavar='size', type=int, default=11, choices=range(1, 101),
                   help='Size of the area sides (default=11)')
parser.add_argument('-S','--scale', metavar='scale', type=int, default=2, choices=range(1, 3),
                    help="Google scale factor. '1' for 640x640, or '2' for 1280x1280")
parser.add_argument('--key', dest='key', action='store_true',
					help='Look for an API key written in a file named KEY')
parser.add_argument('--no-key', dest='key', action='store_false',
					help='Use default download mode based on IP address')
parser.set_defaults(key=False)

args = parser.parse_args()


#### We take the example of Athis-Mons (91200), France

# !!!!!!!!!!!! NOT USED FOR THE MOMENT !!!!!!!!! 
# !!!!!!!!!!!! will be used in the pipeline with wget_automator.sh !!!!!!!!!
# Data directory path  
# directory_path="data/48.7092991_2.3848041_11x11/"
# os.chdir(directory_path)
# print os.getcwd()

# answ = raw_input("Is it the directory where you want to save your pictures? (Y/N):\n")

# if(answ == 'Y'):
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if(not args.key):
	print "=== Default download mode based on IP address (1000 images/day) ==="
else:
	print "=== Advanced download mode with API key (25.000 images/day) ==="
	try:
		key=open('KEY','r').readline()
                #Remove heading and trailing whitespaces, such as return char at the end of string.
                key=key.strip()
		print key
	except IOError:
		args.key = False
		print "WARNING -> No KEY file in the current directory."
		print "Back to default download mode based on IP address (1000 images/day)"

print "\n"


# Starting point (city center)
# Is there a tennis court around this point? Otherwise we should update it to something with a tennis court.
startingCoordinates = [args.lat,args.long]

# Athis-Mons tennis center
#startingCoordinates = [48.7092991,2.3848041]
# Alternative starting point in Canberra, with a few tennis courts
#startingCoordinates = [48.7092991,2.3848041]

# Scale desired
scale = args.scale
if scale == 1:
        resolution_string="_640_640_"
elif scale == 2:
        resolution_string="_1280_1280_"
else:
        resolution_string="_xxxx_xxxx_"

# Zoom desired
zoomLevel = args.zoom

# Size of the cube for a whole city
cubeSides = args.size

# Html addresses format
if(not args.key):
	dl_url = "https://maps.googleapis.com/maps/api/staticmap?scale="+\
        str(scale)+"&zoom="+\
	str(zoomLevel)+"&size=640x640&maptype=satellite&center="
else:
	dl_url = "https://maps.googleapis.com/maps/api/staticmap?scale="+\
        str(scale)+"&zoom="+\
	str(zoomLevel)+"&size=640x640&maptype=satellite&key="+str(key)+"&center="

gps = open("x_y_latitude_longitude", "w")
wget = open("wget_dl_addresses", "w")
names = open("names_list", "w")

g = GpsCoordinate(startingCoordinates[0], startingCoordinates[1])
print "##### CENTER #####"
g.printGpsCoordinate()
g.moveGpsCoordinate("W", 500)
g.moveGpsCoordinate("N", 500)
print "##### 0 / 0 #####"
for i in range(0,cubeSides):
	for j in range(0,cubeSides):
		latitude, longitude = g.getCoordinates()
		print (str(i)+"\t"+str(j)+"\t"+str(latitude)+"\t"+str(longitude))
		gps.write(str(i)+"\t"+str(j)+"\t"+str(latitude)+"\t"+str(longitude)+"\n")
		print (dl_url+str(latitude)+","+str(longitude))
		wget.write(dl_url+str(latitude)+","+str(longitude)+"\n")
		print (str(i)+"_"+str(j)+resolution_string+"zoom"+str(zoomLevel)+".png")
		names.write(str(i)+"_"+str(j)+resolution_string+"zoom"+str(zoomLevel)+".png\n")
		g.moveGpsCoordinate("E", 100)
	g.moveGpsCoordinate("W", 100*cubeSides)
	g.moveGpsCoordinate("S", 100)

gps.close()
wget.close()
names.close()
