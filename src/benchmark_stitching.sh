#!/bin/sh
VERSION=0.1
# Benchmark script to record stitching performances using openCV

EXE=$1
DATA_PATH=$2
echo "Executable:"
echo $EXE
echo "Data directory:"
echo $DATA_PATH

# echo "------------- SQUARE -------------"
# echo "1st test with 10 times 4 images processing"
# time for i in `seq 1 10`
# do
# 	echo $i
# 	./$EXE --output ${DATA_PATH}/result_${i}_0-4_square.jpg ${DATA_PATH}/0_0_1280_1280_zoom19.png ${DATA_PATH}/0_1_1280_1280_zoom19.png ${DATA_PATH}/1_0_1280_1280_zoom19.png ${DATA_PATH}/1_1_1280_1280_zoom19.png
# done > log_perfs_10x4_square_detailed_optim

# echo "------------- LINE -------------"
# echo "1st test with 10 times 4 images processing"
# time for i in `seq 1 10`
# do
# 	echo $i
# 	./$EXE --output ${DATA_PATH}/result_line1.jpg ${DATA_PATH}/$0_0_1280_1280_zoom19.png ${DATA_PATH}/0_1_1280_1280_zoom19.png 
# 	./$EXE --output ${DATA_PATH}/result_line2.jpg ${DATA_PATH}/1_2_1280_1280_zoom19.png ${DATA_PATH}/${i}_3_1280_1280_zoom19.png
# 	./$EXE --output ${DATA_PATH}/result_${i}_0-4_square_lbl.jpg ${DATA_PATH}/result_line1.jpg ${DATA_PATH}/result_line2.jpg
# done > log_perfs_10x4_lbl_square_detailed_optim


# echo "------------- SQUARE -------------"
# time for i in `seq 1 10`
# do
# 	echo $i
# 	./$EXE --output ${DATA_PATH}/result_${i}_0-9_square_optim.jpg ${DATA_PATH}/0_0_1280_1280_zoom19.png ${DATA_PATH}/0_1_1280_1280_zoom19.png ${DATA_PATH}/0_2_1280_1280_zoom19.png ${DATA_PATH}/1_0_1280_1280_zoom19.png ${DATA_PATH}/1_1_1280_1280_zoom19.png ${DATA_PATH}/1_2_1280_1280_zoom19.png ${DATA_PATH}/2_0_1280_1280_zoom19.png ${DATA_PATH}/2_1_1280_1280_zoom19.png ${DATA_PATH}/2_2_1280_1280_zoom19.png 
# done > log_perfs_10x9_square
# echo "------------------------------"

echo "TEST WITH LINE BY LINE SPLITTING"
time for i in `seq 1 10`
do
	echo $i
	./$EXE --pano --output ${DATA_PATH}/result_line1.jpg ${DATA_PATH}/0_0_1280_1280_zoom19.png ${DATA_PATH}/0_1_1280_1280_zoom19.png ${DATA_PATH}/0_2_1280_1280_zoom19.png
	./$EXE --pano --output ${DATA_PATH}/result_line2.jpg ${DATA_PATH}/1_0_1280_1280_zoom19.png ${DATA_PATH}/1_1_1280_1280_zoom19.png ${DATA_PATH}/1_2_1280_1280_zoom19.png
	./$EXE --pano --output ${DATA_PATH}/result_line3.jpg ${DATA_PATH}/2_0_1280_1280_zoom19.png ${DATA_PATH}/2_1_1280_1280_zoom19.png ${DATA_PATH}/2_2_1280_1280_zoom19.png
	# ./stitching --horizontal --output ${DATA_PATH}/result_column1.jpg ${DATA_PATH}/0_2_1280_1280_zoom19.png ${DATA_PATH}/1_2_1280_1280_zoom19.png ${DATA_PATH}/2_2_1280_1280_zoom19.png
	./$EXE --horizontal --pano --output ${DATA_PATH}/result_${i}_0-9_square_lbl_optim.jpg ${DATA_PATH}/result_line1.jpg ${DATA_PATH}/result_line2.jpg ${DATA_PATH}/result_line3.jpg
done > log_perfs_10x9_lbl_square
echo "------------------------------"

# echo "3rd test with 10 times 16 images processing"
# echo "------------- SQUARE -------------"
# time for i in `seq 1 10`
# do
# 	echo $i
# 	./stitching --output ${DATA_PATH}/result_${i}_0-16_square.jpg ${DATA_PATH}/0_0_1280_1280_zoom19.png ${DATA_PATH}/0_1_1280_1280_zoom19.png ${DATA_PATH}/0_2_1280_1280_zoom19.png ${DATA_PATH}/0_3_1280_1280_zoom19.png ${DATA_PATH}/1_0_1280_1280_zoom19.png ${DATA_PATH}/1_1_1280_1280_zoom19.png ${DATA_PATH}/1_2_1280_1280_zoom19.png ${DATA_PATH}/1_3_1280_1280_zoom19.png ${DATA_PATH}/2_0_1280_1280_zoom19.png ${DATA_PATH}/2_1_1280_1280_zoom19.png ${DATA_PATH}/2_2_1280_1280_zoom19.png ${DATA_PATH}/2_3_1280_1280_zoom19.png ${DATA_PATH}/3_0_1280_1280_zoom19.png ${DATA_PATH}/3_1_1280_1280_zoom19.png ${DATA_PATH}/3_2_1280_1280_zoom19.png ${DATA_PATH}/3_3_1280_1280_zoom19.png
# done > log_perfs_10x16_square
# echo "------------------------------"
# echo ""
# echo "TEST WITH LINE BY LINE SPLITTING"
# time for i in `seq 1 10`
# do
# 	echo $i
# 	./stitching --output ${DATA_PATH}/result_line1.jpg ${DATA_PATH}/0_0_1280_1280_zoom19.png ${DATA_PATH}/0_1_1280_1280_zoom19.png ${DATA_PATH}/0_2_1280_1280_zoom19.png ${DATA_PATH}/0_3_1280_1280_zoom19.png 
# 	./stitching --output ${DATA_PATH}/result_line2.jpg ${DATA_PATH}/1_0_1280_1280_zoom19.png ${DATA_PATH}/1_1_1280_1280_zoom19.png ${DATA_PATH}/1_2_1280_1280_zoom19.png ${DATA_PATH}/1_3_1280_1280_zoom19.png 
# 	./stitching --output ${DATA_PATH}/result_line3.jpg ${DATA_PATH}/2_0_1280_1280_zoom19.png ${DATA_PATH}/2_1_1280_1280_zoom19.png ${DATA_PATH}/2_2_1280_1280_zoom19.png ${DATA_PATH}/2_3_1280_1280_zoom19.png 
# 	./stitching --output ${DATA_PATH}/result_line4.jpg ${DATA_PATH}/3_0_1280_1280_zoom19.png ${DATA_PATH}/3_1_1280_1280_zoom19.png ${DATA_PATH}/3_2_1280_1280_zoom19.png ${DATA_PATH}/3_3_1280_1280_zoom19.png
# 	./stitching --horizontal --output ${DATA_PATH}/result_${i}_0-16_lbl_square.jpg ${DATA_PATH}/result_line1.jpg ${DATA_PATH}/result_line2.jpg ${DATA_PATH}/result_line3.jpg ${DATA_PATH}/result_line4.jpg
# done > log_perfs_10x16_lbl_square
# echo "------------------------------"