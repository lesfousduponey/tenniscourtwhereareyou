#!/bin/sh
VERSION=0.1
# basic stitching script. To run, simply call it with the list of pictures to stitch together (order does not matter at this stage)
# example: ./stitchmosaic.sh *.png
PROJECT_NAME='project'
OUTPUT_PREFIX="prefix"

function usage(){
    echo "Usage: $0 [OPTIONS]... INPUT..."
    echo "Stitch INPUT images together"
    echo ""
    echo "INPUT... are image filenames, order is not important"
    echo ""
    echo "Options:"
    echo -e "\t-h\t\t\tprint this help message and exit"
    echo -e "\t-V\t\t\tprint the version of this script and exit"
    echo -e "\t-p PROJECT_NAME\t\tSet the project name for intermediary files (default is 'project')"
    echo -e "\t-o PREFIX\t\tSet the output prefix string for generated image filename (default is 'prefix')"
    echo "Examples:"
    echo "$0 *.png"
    echo "Will stitch all the image files together"
    echo "Intermediate project files and image files are kept for inspection"
    echo "Seems to work fine for 3x3, and 4x4. Starts to have issues above that"
}
#TODO add an option to remove intermediary files, or better, not create them if possible

function version(){
    echo "Version: ${VERSION}"
}
echo "$@"

# Reading options here. Tutorials on getopts are usefull to understand.
while getopts ":hVp:o:" opt
do
    case $opt in
	h)
	    usage
	    exit
	    ;;
	V)
	    version
	    exit
	    ;;
	p)
	    PROJECT_NAME=$OPTARG
	    ;;
	o)
	    OUTPUT_PREFIX=$OPTARG
	    ;;
	\?)
	    usage
	    exit
	    ;;
    esac
done
echo $PROJECT_NAME
echo $OUTPUT_PREFIX
#echo "$@"
shift $(expr $OPTIND - 1 )
#echo "$@"
LIST_OF_INPUT_IMAGES="$@"

# put all your pictures in a folder and run this script

# Create a project with all the pictures. -p 0 is for rectilinear -f 10 is for hfov value
pto_gen -o ${PROJECT_NAME}.pto -p 0 -f 10 -s 1 ${LIST_OF_INPUT_IMAGES}

echo "******************************************************************** GENERATING CONTROL POINT"
# generate control points. There are a few different way to do it, not sure which one is the best (fastest one)
cpfind --multirow -o ${PROJECT_NAME}_cp.pto ${PROJECT_NAME}.pto
#there might be a way to go faster with the prealigned option which is not documented in the man. To try

echo "******************************************************************** SETTING VARS"
#pto_var is used here to say what to optimize with more precision
pto_var --output ${PROJECT_NAME}_param.pto --opt TrX,TrY,TrZ,y,p,r,Tpy,Tpp ${PROJECT_NAME}_cp.pto
#optimize
echo "******************************************************************** OPTIMIZING"
#autooptimiser -n -o ${PROJECT_NAME}_opt.pto ${PROJECT_NAME}_param.pto
autooptimiser -a -l -s -o ${PROJECT_NAME}_opt.pto ${PROJECT_NAME}_param.pto


echo "******************************************************************** SIZE CROP"
# Calculate optimal crop and optimal size
# We might want to specify the canvas if we want to get a specific resolution, usefull if doing stitch of stitch.
pano_modify -o ${PROJECT_NAME}_cs.pto --center --straighten --canvas=AUTO --crop=AUTO ${PROJECT_NAME}_opt.pto

echo "******************************************************************** CREATING THE JOB"
# Create stitching job
pto2mk -o ${PROJECT_NAME}.mk -p ${OUTPUT_PREFIX} ${PROJECT_NAME}_cs.pto
echo ${OUTPUT_PREFIX}

echo "******************************************************************** STITCHING"
# Stitch the job
make -f ${PROJECT_NAME}.mk all
