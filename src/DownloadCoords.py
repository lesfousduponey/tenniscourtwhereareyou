# Copyright 2014 TCWAY
# This file is part of TCWAY (Tennis Court Where Are You).
#
# TCWAY is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TCWAY is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TCWAY.  If not, see <http://www.gnu.org/licenses/>.

# Author: Mikael T., Christophe S.

import sys
import subprocess
from subprocess import Popen

# Arguments parsing
latitude = sys.argv[1]
longitude = sys.argv[2]
zoomLevel = sys.argv[3]
size = sys.argv[4]
output_file = sys.argv[5]

# Download mode choice (with or without APi key)
key = raw_input("If you want to download images with your API key, please enter it, if not, type \"N\":\n")
if(key=='N'):
	print "=== Default download mode based on IP address (1000 images/day) ===\n"
	dl_url = "https://maps.googleapis.com/maps/api/staticmap?scale=2&zoom="+\
	str(zoomLevel)+"&size="+str(size)+"x"+str(size)+"&maptype=satellite&\
	center="+str(latitude)+","+str(longitude)
else:
	print "=== Advanced download mode with API key (25.000 images/day) ===\n"
	dl_url = "https://maps.googleapis.com/maps/api/staticmap?scale=2&zoom="+\
	str(zoomLevel)+"&size="+str(size)+"x"+str(size)+"&maptype=satellite&key="\
	+str(key)+"&center="+str(latitude)+","+str(longitude)

# Summary of download parameters
summary = "The image with GPS coordinates of "+str(latitude)+","+str(longitude)+"\
 at zoom "+str(zoomLevel)+" and size "+str(size)+" will be saved in "+output_file+"\n"

print summary
answ = raw_input("Are all parameters well set up ? (Y to continue / N to abort):\n")

if(answ == 'Y'):
	command = ['wget', dl_url, '-O', output_file ]
	p = Popen(command, stdout=subprocess.PIPE)
	stdout, stderr = p.communicate()

print "Done"