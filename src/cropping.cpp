// Copyright 2014 TCWAY
// This file is part of TCWAY (Tennis Court Where Are You).
//
// TCWAY is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TCWAY is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with TCWAY.  If not, see <http://www.gnu.org/licenses/>.

// Author: Christophe S. / Mikael T.

#include <unistd.h>
#include <cv.h>
#include <highgui.h>
#include "cropping.hpp"

using namespace std;
using namespace cv;

#define DEFAULT_POSTFIX "_crop"
#define DEFAULT_PIXCUT 50

cropImage::cropImage(string filename, string postfix, int pixel_to_cut)
  : pixcut(pixel_to_cut), filename(filename), postfix(postfix) {

  // Dealing with file extension to be able to append the postfix
  int lastindex = filename.find_last_of(".");
  if (lastindex == -1) {
	cout << "Couldn't detect image file extension, assuming .jpg" << endl;
	set_crop_filename(filename + postfix + ".jpg");
  }
  else {
	string rawname = filename.substr(0, lastindex);
	string extension = filename.substr(lastindex, filename.length());
	set_crop_filename(rawname + postfix + extension);
  }

}

void
cropImage::set_crop_filename(string name) {
  this->crop_filename = name;
}

string
cropImage::get_filename() const{
  return this->filename;
}

string
cropImage::get_postfix() const{
  return this->postfix;
}

string
cropImage::get_crop_filename() const{
  return this->crop_filename;
}

int
cropImage::get_pixcut() const{
  return this->pixcut;
}

int
cropImage::cropit() {
  // Read the image
  Mat img = imread(this->filename);

  // Check the image is not empty
  if (img.empty()) {
	cout << "Can't read image '" << this->filename << "'\n";
	return -1;
  }

  // Get the resultion if the image
  cv::Size s = img.size();

  // Create a rectangular covvering the image portion we want to keep
  Rect roi(0, 0, s.width, s.height - this->pixcut);

  // This clone the image, keeping online the portion of the original we want to keep
  Mat croppedImage = img(roi).clone();

  // Write back
  imwrite(this->crop_filename, croppedImage);
  return 0;
}

ostream& operator<<(ostream& out, const cropImage& c){
  return out << "Filename is '" + c.get_filename() + "'" << endl
			 << "Postfix is '" + c.get_postfix() + "'" << endl
			 << "Number of pixel to cut are '" << c.get_pixcut() << "'" << endl
			 << "New filename is '" + c.get_crop_filename() + "'";
}

void printUsage(char * prog_name)
{
  cout <<
	"Usage: " << prog_name << " [OPTIONS]... INPUT\n"
	"Crop bottom of INPUT images.\n"
	"Options:\n"
	"\t-n\t\tNumber of pixels to crop at the bottom.\n"
	"\t\t\tThe default is '" << DEFAULT_PIXCUT << "'.\n"
	"\t-p <postfix>\tPostfix for all the resulting cropped images.\n"
	"\t\t\tThe default is '" << DEFAULT_POSTFIX << "'.\n"
	"\t-h\t\tPrint this help message.\n"
	"Examples:\n" << prog_name << " -n 10 -p test *.png"
	"\n";
}

vector<cropImage>
parseCmdArgs(int argc, char** argv) {

  int option_char;
  string postfix_name = DEFAULT_POSTFIX;
  int pixel_to_cut = DEFAULT_PIXCUT;
  int index;
  vector<cropImage> vec_cropimage;

  while ((option_char = getopt(argc, argv, ":hp:n:")) != -1) {
    switch (option_char) {
	case 'h': {
	  printUsage(argv[0]);
	  exit(-1);
	}
	case 'p': {
	  postfix_name.assign(optarg);
	  cout << "Going to use '" << postfix_name << "' as a postfix"<< endl;
	  break;
	}
	case 'n' : {
	  pixel_to_cut = atoi(optarg);
	  cout << "Going to cut " << pixel_to_cut << " pixels" << endl;
	  break;
	}
	case '?' : {
	  cout << "Error in the command line argument: option not recognized." << endl;
	  printUsage(argv[0]);
	  exit(-1);
	}
	case ':' : {
	  cout << "Error in the command line argument: missing parameter." << endl;
	  printUsage(argv[0]);
	  exit(-1);
	}
	}
  }

  for (index = optind; index < argc; index++) {
	cropImage a = cropImage(argv[index], postfix_name, pixel_to_cut);
	vec_cropimage.push_back(a);
  }
  return vec_cropimage;
}

int
main(int argc, char* argv[]) {

  // Read command line and build a vector of cropImage, one
  // for each image to crop.
  vector<cropImage> vec_cropimage(parseCmdArgs(argc, argv));

  // Iterate over the vector to crop each image
  for (vector<cropImage>::iterator it = vec_cropimage.begin();
	   it != vec_cropimage.end();
	   it++) {
	//	cout << *it << endl << endl;
	(*it).cropit();
  }
  return 1;
}
