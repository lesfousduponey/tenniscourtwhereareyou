import math

class ZoomToMeters:

    def __init__(self, zoom, latitude, widthInPixels):
        self.zoomLevel = zoom
        self.latitude = latitude
        self.widthInPixels = widthInPixels

    def calculateZoomLevel(screenWidth):
        equatorLength = 40075004 # in meters
        widthInPixels = screenWidth
        metersPerPixel = equatorLength / 256
        zoomLevel = 1
        while ((metersPerPixel * widthInPixels) > 2000):
            metersPerPixel /= 2
            zoomLevel+=1
        print "zoom level = %d" % zoomLevel
        return zoomLevel

    def calculateDistance(self):
        ## 1st method
        # equatorLength = 40075004 # in meters
        # widthInPixels = 640
        # metersPerPixel = equatorLength / 256
        # distance = ( (metersPerPixel / math.pow(2, self.zoomLevel)) * widthInPixels)
        # print "distance = %f" % distance
        #return distance

        ## 2nd method
        earthRadius = 6378137 # in meters
        tileSizeZoom0 = earthRadius * 2 * math.pi / 256
        resolution = tileSizeZoom0 * math.cos(self.latitude * math.pi / 180) / math.pow(2, self.zoomLevel)
        print "resolution = %f" % resolution
        surface_m = (resolution * self.widthInPixels)*(resolution * self.widthInPixels)
        surface_km = surface_m  / 1000000
        print "surface = %f km2 / %f m2" % (surface_km, surface_m)
        return surface_m

z = ZoomToMeters(19, 48.7107919739, 640)
#zoom = calculateZoomLevel(1280)
distance = z.calculateDistance()
