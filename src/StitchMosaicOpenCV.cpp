// Copyright 2014 TCWAY
// This file is part of TCWAY (Tennis Court Where Are You).
//
// TCWAY is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TCWAY is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with TCWAY.  If not, see <http://www.gnu.org/licenses/>.

// Author: Mikael T., Christophe S.

#include <iostream>
#include <fstream>
#include <chrono>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/stitching/stitcher.hpp"
#include "opencv2/gpu/gpu.hpp"

using namespace std;
using namespace cv;

void usage()
{
	cout <<
	        "Rotation model images stitcher.\n\n"
	        "stitching img1 img2 [...imgN]\n\n"
	        "Flags:\n"
	        "  --try_use_gpu (yes|no)\n"
	        "      Try to use GPU. The default value is 'no'. All default values\n"
	        "      are for CPU mode.\n"
	        "  --output <result_img>\n"
	        "      The default is 'result.jpg'.\n";
}

bool try_use_gpu = false;
bool horizontal = false;
vector<Mat> imgs;
string result_name = "result.jpg";

int parseArgs(int argc, char** argv)
{
	if(argc == 1)
	{
		usage();
		return -1;
	}

	for (int i = 1; i < argc; ++i)
    {
        if (string(argv[i]) == "--help" || string(argv[i]) == "/?")
        {
            usage();
            return -1;
        }
        else if (string(argv[i]) == "--try_use_gpu")
        {
            if (string(argv[i + 1]) == "no")
                try_use_gpu = false;
            else if (string(argv[i + 1]) == "yes")
            {
            	try_use_gpu = true;
            	int nb_gpu = gpu::getCudaEnabledDeviceCount(); 
		        gpu::DeviceInfo info = gpu::getDevice(); 
		        cout << "Number of CUDA gpu: " << nb_gpu << "\nDevice name: " << info.name() << endl; 
            }
            else
            {
                cout << "Bad --try_use_gpu flag value\n";
                return -1;
            }
            i++;
        }
        else if (string(argv[i]) == "--output")
        {
            result_name = argv[i + 1];
            i++;
        }
        else if (string(argv[i]) == "--horizontal")
        {
            horizontal = true;
        }
        else
        {
        	cout << "Recording image: " << argv[i] << endl;
            Mat img = imread(argv[i]);
            if (img.empty())
            {
                cout << "== ERROR == Can't read image '" << argv[i] << "'\n";
                return -1;
            }
            imgs.push_back(img);
        }
    }
    return 0;
}

int main(int argc, char* argv[])
{
    int retval = parseArgs(argc, argv);
    if (retval) return -1;

    auto t1 = std::chrono::high_resolution_clock::now();

    if(horizontal)
    {
        for(vector<Mat>::iterator i=imgs.begin(); i!=imgs.end(); i++)
        {
            Mat tmp;
            cv::transpose(*i, tmp);
            *i=tmp;
        }
    }

    Mat mosaic;
    Stitcher stitcher = Stitcher::createDefault(try_use_gpu);
    Stitcher::Status status = stitcher.stitch(imgs, mosaic);

    if (status != Stitcher::OK)
    {
        cout << "Can't stitch images, error code = " << status << endl;
        return -1;
    }

    if(horizontal)
    {
        Mat tmp;
        cv::transpose(mosaic, tmp);
        mosaic = tmp;
    }

    cout << "Writing final image " << result_name << endl;
    imwrite(result_name, mosaic);

    auto t2 = std::chrono::high_resolution_clock::now();

    std::cout << "stitching() took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
              << " milliseconds\n";

    return 0;
}
