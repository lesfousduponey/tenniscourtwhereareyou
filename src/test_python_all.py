import random
import unittest
from GpsCoordinate import *

"""
For the moment, we could use this file to write tests. One class per python file?
When it grows we will probably split it
To run all tests, simply type:
python test_python_all.py

To run only a specific tests, such as testGoBack from TestGpsCoordinate class:
python test_python_all.py TestGpsCoordinate.testGoBack
OR
python -m unittest test_python_all.TestGpsCoordinate.testGoBack

To get help on unittest options: python -m unittest -h

Would be nice to write git hook at some points to run automatically before or after a commit for example.
"""

class TestGpsCoordinate(unittest.TestCase):
    gps_epsilon = 0.00002 # correspond to 2 m precision
    direction = ['N', 'S', 'E', 'W', 'north', 'south', 'east', 'west', 'NORTH', 'SOUTH', 'EAST', 'WEST', 'North', 'South', 'East', 'West']
    return_direction = {'N':'S', 'S':'N', 'E':'W', 'W':'E', 'north':'S', 'south':'N', 'east':'W', 'west':'E', 'NORTH':'S', 'SOUTH':'N', 'EAST':'W', 'WEST':'E', 'North':'S', 'South':'N', 'East':'W', 'West':'E'}
    print "Tests on GpsCoordinate with %.1fm precision" % (gps_epsilon*100000)
    def testBasic(self):
        """
        cover basic usage
        """
        print "\nTest basic translation from starting point (44, 5) to north, 300m away"
        g = GpsCoordinate(44, 5)
        g.moveGpsCoordinate("N", 300)
        self.assertTrue(math.fabs(g.latitude - 44.0026979648) < self.gps_epsilon)

    def testGoBack(self):
        """
        cover that when going in a direction/distance and coming back, we end in almost the same point
        cover moveGpsCoordinate and nextGpsCoordinate
        """
        print "\nTest if when going to a point and coming back, we end at the same starting position"
        random.seed(666)
        for i in range(1,10000):
            g = GpsCoordinate(random.uniform(-84,84), random.uniform(-180,180))
            nsew_dir = self.direction[random.randint(0, 15)]
            dist = random.uniform(50, 1000)
            h = g.nextGpsCoordinate(nsew_dir, dist)
            h.moveGpsCoordinate(self.return_direction[nsew_dir], dist)
            self.assertTrue(math.fabs(g.latitude - h.latitude) < self.gps_epsilon, "latitude problem with iteration " + str(i))
            self.assertTrue(math.fabs(g.longitude - h.longitude) < self.gps_epsilon, "longitude problem with iteration " + str(i))
    def testSquare(self):
        """
        follow a square and check we end up in almost the same point
        """
        print "\nTest if when doing a square route, we end at the same starting position"
        random.seed(13)
        for i in range(1,10000):
            # google maps does not go beyond latitude of 85
            g = GpsCoordinate(random.uniform(-84,84), random.uniform(-180,180))
            idx_nsew_dir = random.randint(0, 15)
            dist = random.uniform(0, 1000)
            h = g.nextGpsCoordinate(self.direction[idx_nsew_dir], dist)
            h.moveGpsCoordinate(self.direction[(idx_nsew_dir + 1) % 16], dist)
            h.moveGpsCoordinate(self.direction[(idx_nsew_dir + 2) % 16], dist)
            h.moveGpsCoordinate(self.direction[(idx_nsew_dir + 3) % 16], dist)
            self.assertTrue(math.fabs(g.latitude - h.latitude) < self.gps_epsilon, "latitude problem with iteration " + str(i))
            self.assertTrue(math.fabs(g.longitude - h.longitude) < self.gps_epsilon, "longitude problem with iteration " + str(i))
            
if __name__ == '__main__':
    unittest.main()

    
