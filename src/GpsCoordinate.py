# Copyright 2014 TCWAY
# This file is part of TCWAY (Tennis Court Where Are You).
#
# TCWAY is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TCWAY is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TCWAY.  If not, see <http://www.gnu.org/licenses/>.

# Author: Christophe S. / Mikael T.

import math
import copy

class GpsCoordinate:
    # The allowed direction for the purpose of this work
    NSEW_direction = {'N':0, 'North':0, 'NORTH':0, 'north':0,
                      'S':180, 'South':180, 'SOUTH':180, 'south':180,
                      'E':90, 'East':90, 'EAST':90, 'east':90,
                      'W':270, 'West':270, 'WEST':270, 'west':270}
    
    # Radius of the earth, in meter
    Rworld = 6371000.0
    
    # Maximum distance (meter) used for the purpose of this work
    MaxDist = 10000
    
    def __init__(self, latitude, longitude):
        # using asserts at the moment, might do something better latter.
        assert((latitude > -90) & (latitude < 90))
        assert((longitude > -180) & (longitude < 180))
        self.latitude = latitude
        self.longitude = longitude

    def __str__(self):
        # print format ensure the "." is always aligned for nice formating
        # print 10 digit precision. That's actually too mcuh (sub millimeter precision)
        return "%16.10f %16.10f" % (self.latitude, self.longitude)

    def __repr__(self):
        #return "GpsCoordinage(%16.10 %16.10)" % (self.latitude, self.longitude)
        return "GpsCoordinate(%.10f,%.10f)" % (self.latitude, self.longitude)

    def getCoordinates(self):
        return self.latitude, self.longitude

    # DEPRECATED, use print instead (__str__)
    def printGpsCoordinate(self):
        # print format ensure the "." is always aligned for nice formating
        # print 10 digit precision. That's actually too mcuh (sub millimeter precision)
        print "%16.10f %16.10f" % (self.latitude, self.longitude)

    def moveGpsCoordinate(self, direction, distance):
        """
        This method 'move' the GPS coordinate to its new location given
        a direction and a distance.
        direction: should be one of the N, S, E, W values
        distance: a distance in meter.
        """
        assert (distance > 0) & (distance < self.MaxDist), "Distance should be positives, and not too big"
        assert direction in self.NSEW_direction, "Direction should be one of 'N' 'North' 'north' NORTH' etc..."
        
        # Calculation of the new longitude and latitude
        # Inspired by http://www.movable-type.co.uk/scripts/latlong.html
        rlon = math.radians(self.longitude)
        rlat = math.radians(self.latitude)
        b = math.radians(self.NSEW_direction[direction])
        d = distance / GpsCoordinate.Rworld
        new_rlat = math.asin(math.sin(rlat) * math.cos(d)
                            + math.cos(rlat) * math.sin(d) * math.cos(b))
        new_rlon = rlon + math.atan2(math.sin(b) * math.sin(d) * math.cos(rlat),
                                     math.cos(d) - math.sin(rlat)*math.sin(new_rlat))
        self.latitude = math.degrees(new_rlat)
        self.longitude = math.degrees(new_rlon)

    def nextGpsCoordinate(self, direction, distance):
        """
        This method 'create' a new GPS coordinate in the distance / direction given
        from the current GpsCoordinate
        direction: should be one of the N, S, E, W values
        distance: a distance in meter.
        """
        new = copy.copy(self)
        new.moveGpsCoordinate(direction, distance)
        return(new)
    
if __name__ == '__main__':
    g = GpsCoordinate(44, 5)
    print g
    g.moveGpsCoordinate("S", 500)
    print g
    h = g.nextGpsCoordinate("W", 500)
    print h, g

"""
The page http://www.movable-type.co.uk/scripts/latlong.html
contains some web script used to validate the code. It works!
"""
